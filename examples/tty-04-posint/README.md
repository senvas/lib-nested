# tty-04-posint

In the following example, a <List Char> editor
as before is used, but its data is morphed into
representing a positional integer which is then
projected into different radices and displayed
in different views on screen
