# tty-02-digit

This example demonstrates how a very simple editor for hexadecimal digits
can be created with `lib-nested` and the `lib-nested-tty` backend.
