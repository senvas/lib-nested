# tty-01-hello

This example shows how to:
 - initialize the TTY backend (`lib-nestetd-tty`),
 - create a simple 'Hello World' output,
 - create color gradients on the outputted text
   utilizing basic projection functionality from `lib-r3vi`,
 - perform basic layouting & compositing.
