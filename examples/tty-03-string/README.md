# tty-03-string

Similarly to `tty-02-digit`, a editor is created
but of type <List Char>.
The contents of the editor can be retrieved by
a morphism from the `EditTree` node.
To demonstrate that, the values are are mapped
to the TTY-display in different form.
